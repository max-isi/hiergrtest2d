import numpy as np

def snr_pdf(snr):
    """Distribution of SNRs, i.e. PDF(snr)"""
    return 3.*12.**3/snr**4


def gaussian(x, mean, sigma):
    """Gaussian PDF(x | mu, sigma)"""
    return 1./sqrt(2.*pi)/sigma*exp(-(x-mean)**2/2./sigma**2)


def MakeSNRPopulation(Ndet, seed, snr_min=12., snr_max=1000.):
    """Draw `Ndet` SNRs from SNR PDF using rejection sampling.
    """
    np.random.seed(seed)
    snr_sample_list = []
    for i in range(Ndet):
        snr_sample = 10000.
        # rejection sampling
        while snr_pdf(snr_sample) < np.random.uniform(0, 0.25):
            snr_sample = np.random.uniform(snr_min, snr_max)
        snr_sample_list.append(snr_sample)
    return np.array(snr_sample_list)
